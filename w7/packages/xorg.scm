(define-module (w7 packages xorg)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xorg)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match))

(define-public x2x
  (package
    (name "x2x")
    (version "1.31-alpha") ;; 1.30 is the lastest release, this is master, not yet released.
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dottedmag/x2x")
             (commit "master")))
       (sha256
        (base32 "0m5yrn01jzlkvxah3ax50h1s37jxdivjrzgfa9jydxikyv7pcj8m"))))
    (build-system gnu-build-system)
    (native-inputs
     (list automake
           autoconf
           autoconf-archive
           pkg-config))
    (inputs
     (list libxext libxtst))
    (home-page "https://github.com/dottedmag/x2x")
    (synopsis "pipe X to another X")
    (description "")
    (supported-systems '("x86_64-linux" "i686-linux"))
    (license license:bsd-3)))
