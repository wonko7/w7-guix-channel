(define-module (w7 packages w7-st)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (w7 packages))

(define-public w7-st
  (package
    (inherit st)
    (name "w7-st")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/wonko7/st")
          (commit "master")))
        (sha256
          (base32 "0z3d22mlp1akwiazlhy7999b1qfxylwsylssyp5925lyhz7jxmpz"))))
    (arguments
     `(#:tests? #f                      ; no tests
       #:make-flags
       (list (string-append "CC=" ,(cc-for-target))
             (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (inputs
      `(("libx11" ,libx11)
        ("libxft" ,libxft)
        ("ncurses" ,ncurses)
        ("fontconfig" ,fontconfig)
        ("freetype" ,freetype)))
    (native-search-paths
      (list (search-path-specification
              (variable "TERMINFO_DIRS")
              (files '("share/terminfo")))))
    (home-page "https://gitlab.com/wonko7/st")
    (synopsis "wonko7's version of st")))
