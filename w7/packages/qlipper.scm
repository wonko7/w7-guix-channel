(define-module (w7 packages qlipper)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system qt)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages lxqt)
  #:use-module (srfi srfi-1))


(define-public qlipper
  (package
    (name "qlipper")
    (version "5.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/pvanek/qlipper/archive/"
             version ".tar.gz"))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32
         "0vbhiyn56qwlssavim02kp0y5rxj6gdffchyigkhpg8qza64afch"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check))))
    (native-inputs
     `(("lxqt-build-tools" ,lxqt-build-tools)
       ("qtlinguist" ,qttools-5)))
    ;(inputs
    ; `(("qtbase" ,qtbase-5)
    ;   ("qtlinguist" ,qttools)))
    (home-page "https://github.com/pvanek/qlipper")
    (synopsis "Lightweight and cross-platform clipboard history applet")
    (description "Lightweight and cross-platform clipboard history applet")
    (license license:gpl2+)))

qlipper
