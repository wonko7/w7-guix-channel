(define-module (w7 packages jonaburg-picom)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages compton)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (w7 packages))

(define-public jonaburg-picom
  (package
    (inherit picom)
    (name "jonaburg-picom")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/jonaburg/picom")
          (commit "e553e00f48de67d52fe75de9e0e940d85aa14a24")))
        (sha256
          (base32 "04svbv7v73q8yn9la69451rda6l2pgxcphv2zlkdqaxxdbp69195"))))
    (home-page "https://github.com/jonaburg/picom")))

(define-public ibhagwan-picom
  (package
    (inherit picom)
    (name "ibhagwan-picom")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/ibhagwan/picom")
          (commit "c4107bb6cc17773fdc6c48bb2e475ef957513c7a")))
        (sha256
          (base32 "035fbvb678zvpm072bzzpk8h63npmg5shkrzv4gfj89qd824a5fn"))))
    (home-page "https://github.com/ibhagwan/picom")))
