(define-module (w7 packages rust-notif)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-26))


(define-public rust-darling-macro-0.13
  (package
    (name "rust-darling-macro")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k0mky8zrz9ayzmjw2yp6l7jbh4ysimrq24zsgkfa3qk8zqvzrxd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.13)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "Internal support for a proc-macro library for reading attributes into structs when
implementing custom derives. Use https://crates.io/crates/darling in your code.
")
    (description
      "Internal support for a proc-macro library for reading attributes into structs when
implementing custom derives.  Use https://crates.io/crates/darling in your code.
")
    (license license:expat)))

(define-public rust-fnv-1
  (package
    (name "rust-fnv")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fnv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hc2mcqha06aibcaza94vbi81j6pr9a1bbxrxjfhc91zin8yr7iz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/servo/rust-fnv")
    (synopsis
      "Fowlerâ\x80\x93Nollâ\x80\x93Vo hash function")
    (description
      "Fowlerâ\x80\x93Nollâ\x80\x93Vo hash function")
    (license (list license:asl2.0 license:expat))))

(define-public rust-darling-core-0.13
  (package
    (name "rust-darling-core")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qv6cjcrl2sb9502f0v8pa41ag66wmshzs7cc1zjs31dnvpxhd1c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fnv" ,rust-fnv-1)
         ("rust-ident-case" ,rust-ident-case-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-strsim" ,rust-strsim-0.10)
         ("rust-syn" ,rust-syn-1))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "Helper crate for proc-macro library for reading attributes into structs when
implementing custom derives. Use https://crates.io/crates/darling in your code.
")
    (description
      "Helper crate for proc-macro library for reading attributes into structs when
implementing custom derives.  Use https://crates.io/crates/darling in your code.
")
    (license license:expat)))

(define-public rust-darling-0.13
  (package
    (name "rust-darling")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04mvk6yvvw9rb8zwyi680r5n5l9xcana3bjdkirqw7gi5bnhsz3m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.13)
         ("rust-darling-macro" ,rust-darling-macro-0.13))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "A proc-macro library for reading attributes into structs when
implementing custom derives.
")
    (description
      "This package provides a proc-macro library for reading attributes into structs when
implementing custom derives.
")
    (license license:expat)))

(define-public rust-serde-with-macros-1
  (package
    (name "rust-serde-with-macros")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_with_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kyyg9p2k7m4566bqpzmgwbhyg70lqmz4b2rpp43wqjlpms96mp1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.13)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page
      "https://github.com/jonasbb/serde_with/tree/master/serde_with_macros")
    (synopsis "proc-macro library for serde_with")
    (description "proc-macro library for serde_with")
    (license (list license:expat license:asl2.0))))

(define-public rust-doc-comment-0.3
  (package
    (name "rust-doc-comment")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "doc-comment" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "043sprsf3wl926zmck1bm7gw0jq50mb76lkpk49vasfr6ax1p97y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/GuillaumeGomez/doc-comment")
    (synopsis "Macro to generate doc comments")
    (description "Macro to generate doc comments")
    (license license:expat)))

(define-public rust-serde-with-1
  (package
    (name "rust-serde-with")
    (version "1.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_with" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wmw2a67prb29gggihi6gllna2jgs3pjb33kvcb8kp5sd6xzvn8s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-doc-comment" ,rust-doc-comment-0.3)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-with-macros"
          ,rust-serde-with-macros-1))))
    (home-page
      "https://github.com/jonasbb/serde_with")
    (synopsis
      "Custom de/serialization functions for Rust's serde")
    (description
      "Custom de/serialization functions for Rust's serde")
    (license (list license:expat license:asl2.0))))

(define-public rust-zeromq-src-0.1
  (package
    (name "rust-zeromq-src")
    (version "0.1.10+4.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zeromq-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wd2xwc6l7ixwxy3vdjxxhif2yhqp91n0dg44bzcyzw1cv9k74fz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cmake" ,rust-cmake-0.1))))
    (home-page
      "https://github.com/jean-airoldie/zeromq-src-rs")
    (synopsis
      "Source code and logic to build ZeroMQ from source
")
    (description
      "Source code and logic to build ZeroMQ from source
")
    (license (list license:expat license:asl2.0))))

(define-public rust-libsodium-sys-0.2
  (package
    (name "rust-libsodium-sys")
    (version "0.2.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libsodium-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zcjka23grayr8kjrgbada6vwagp0kkni9m45v0gpbanrn3r6xvb"))))
    (build-system cargo-build-system)
    (arguments
      `(; #:skip-build?
        ;#t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-walkdir" ,rust-walkdir-2))
        #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'fix-shebang
              (lambda _
                ;;
                (setenv "SHELL" (which "bash"))
                (setenv "CONFIG_SHELL" (which "bash"))
                (display "lolifuck\n")
                (display (getcwd))
                (display "\nlolifuck\n")
                ;; (substitute* "libsodium/configure"
                ;;   ((".*") ;; (string-append "fuckme-" (which "sh"))
                ;;    "lolifuck"
                ;;    )
                ;;   ;(("= /bin/sh") (string-append "= " (which "sh")))
                ;;   )
                #t)))
        ))
    ;; (inputs `(("libsodium", libsodium)))
    ;; nope it's build & included in the package.
    (home-page
      "https://github.com/sodiumoxide/sodiumoxide.git")
    (synopsis "FFI binding to libsodium")
    (description "FFI binding to libsodium")
    (license (list license:expat license:asl2.0))))

(define-public rust-libzmq-sys-0.1
  (package
    (name "rust-libzmq-sys")
    (version "0.1.8+4.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libzmq-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "103bx92jbnzc76w5i587ns2gyizihp871ifhrki65wd3s8n2fq49"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.53)
         ("rust-cmake" ,rust-cmake-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libsodium-sys" ,rust-libsodium-sys-0.2)
         ("rust-zeromq-src" ,rust-zeromq-src-0.1))))
    (home-page
      "https://github.com/jean-airoldie/libzmq-rs")
    (synopsis "Raw CFFI bindings for libzmq
")
    (description "Raw CFFI bindings for libzmq
")
    (license (list license:expat license:asl2.0))))

(define-public rust-humantime-serde-1
  (package
    (name "rust-humantime-serde")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "humantime-serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0n208zzy69f7pgwcm1d0id4nzhssxn3z3zy7ki3dpkaazmnaad5c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-humantime" ,rust-humantime-2)
         ("rust-serde" ,rust-serde-1))))
    (home-page
      "https://github.com/jean-airoldie/humantime-serde")
    (synopsis
      "Serde support for the `humantime` crate")
    (description
      "Serde support for the `humantime` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-flatc-rust-0.1
  (package
    (name "rust-flatc-rust")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "flatc-rust" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16mxjlwvnvwnnal8lsnkqzv0c744sxb9npi5w02w9k1viv32mkpx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/frol/flatc-rust")
    (synopsis "FlatBuffers flatc command as API")
    (description "FlatBuffers flatc command as API")
    (license (list license:expat license:asl2.0))))

(define-public rust-libzmq-0.2
  (package
    (name "rust-libzmq")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libzmq" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0icz0sf8nmj0nhwqmvdv1105hhfxzwx19prxlrjy32j33jjgvr3y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-flatc-rust" ,rust-flatc-rust-0.1)
         ("rust-humantime-serde" ,rust-humantime-serde-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libzmq-sys" ,rust-libzmq-sys-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-with" ,rust-serde-with-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-uuid" ,rust-uuid-0.8))))
    (home-page
      "https://jean-airoldie.github.io/libzmq-rs/")
    (synopsis
      "A strict subset of Ã\x98MQ with a high level API.
")
    (description
      "This package provides a strict subset of Ã\x98MQ with a high level API.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-notif-0.0.12
  (package
    (name "rust-notif")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notif" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xybk67gmvam3flsagr7nhppszsnp18j98kbspxglcfah8370jxd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-dirs" ,rust-dirs-2)
         ("rust-failure" ,rust-failure-0.1)
         ("rust-hostname" ,rust-hostname-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libzmq" ,rust-libzmq-0.2)
         ("rust-serde" ,rust-serde-1)
         ;("rust-libsodium-sys" ,rust-libsodium-sys-0.2)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-signal-hook" ,rust-signal-hook-0.1)
         ("rust-toml" ,rust-toml-0.5))))
    (home-page "https://github.com/Wonko7/notif")
    (synopsis
      "route remote notifications to current desktop")
    (description
      "route remote notifications to current desktop")
    (license license:gpl2)))


rust-notif-0.0.12
;;rust-libsodium-sys-0.2
